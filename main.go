package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/saltmines/setlist-go-api/login"
	"bitbucket.org/saltmines/setlist-go-api/service"
)

func main() {
	http.HandleFunc("/login", login.LoginHandler)
	http.HandleFunc("/GetConfiguration", service.GetConfigHandler)

	//Port to run on Azure
	http.ListenAndServe(":"+os.Getenv("HTTP_PLATFORM_PORT"), nil)
	log.Fatal(":"+os.Getenv("HTTP_PLATFORM_PORT"), nil)

	//Port to run locally
	//http.ListenAndServe(":8080", nil)
}
