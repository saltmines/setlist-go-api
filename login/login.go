package login

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/saltmines/setlist-go-api/utility"
)

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var loginRequest utility.Initial_Request
	var userinfo utility.Userinfo
	var loginresponse utility.Response_message

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&loginRequest)
	if err != nil {
		panic("Error reading request") //err)
	}
	defer r.Body.Close()
	/*
		l_query_str := fmt.Sprintf(`select userid,email from login where PhoneNumber='%s'`, '9999')
		fmt.Println("query:", l_query_str)
		row, err := db_utility.DBC.Query(l_query_str)
		if err != nil {
			log.Fatal("Prepare failed:", err.Error())
		}

		_, rows, r_err := db_utility.DBScan_fn(row)

		if r_err != nil {
			fmt.Println("no data found err")
			return
		}

		if len(rows) == 0 {
			loginresponse.Errors = "user not found"
		} else {

			userid := rows[0][0]
			email := rows[0][1]
			l_query_str1 := fmt.Sprintf(`select firstname,lastname,phonenumber,userimage from UserDetails where userid=%s`, userid)
			fmt.Println("query:", l_query_str1)
			row1, err1 := db_utility.DBC.Query(l_query_str1)
			if err1 != nil {
				log.Fatal("Prepare failed:", err.Error())
			}

			_, rows1, r_err1 := db_utility.DBScan_fn(row1)

			if r_err1 != nil {
				fmt.Println("no data found err")
				return
			}
	*/
	loginresponse.Errors = "null"
	loginresponse.Result.Httpstatus = 200
	loginresponse.Result.Configuration.Mqttconfig[0].Pi = "40.112.211.216"
	loginresponse.Result.Configuration.Mqttconfig[0].Dwp = "kunila"
	loginresponse.Result.Configuration.Mqttconfig[0].Nsu = "kunila"
	loginresponse.Result.Message = "Logged in Succesfully."
	loginresponse.Success = "true"
	userinfo.Email = loginRequest.Username //email
	userinfo.Firstname = ""                //rows1[0][0]
	userinfo.Lastname = ""                 //rows1[0][1]
	userinfo.Phonenumber = ""              //rows1[0][2]
	userinfo.Userid = ""                   //userid
	userinfo.Userimage = ""                //rows1[0][3]
	loginresponse.Result.Userinfo = &userinfo

	//		loginresponse.Result.Userinfo.Email = email
	//		loginresponse.Result.Userinfo.Firstname = rows1[0][0]
	//		loginresponse.Result.Userinfo.Lastname = rows1[0][1]
	//		loginresponse.Result.Userinfo.Phonenumber = rows1[0][2]
	//		loginresponse.Result.Userinfo.Userid = userid
	//		loginresponse.Result.Userinfo.Userimage = rows1[0][3]
	//}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(loginresponse)

}
