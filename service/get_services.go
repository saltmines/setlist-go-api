package service

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/saltmines/setlist-go-api/utility"
)

func GetConfigHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GetConfigHandler")
	var configresponse utility.Response_message
	configresponse.Errors = "null"
	configresponse.Result.Configuration.Mqttconfig[0].Pi = "40.112.211.216"
	configresponse.Result.Configuration.Mqttconfig[0].Dwp = "kunila"
	configresponse.Result.Configuration.Mqttconfig[0].Nsu = "kunila"
	configresponse.Success = "true"
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(configresponse)
	fmt.Println("api key:", r.Header.Get("api_key"))
}
