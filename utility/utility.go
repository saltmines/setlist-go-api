package utility

type Response_message struct {
	Errors  string `json:"errors"`
	Result  Result `json:"result"`
	Success string `json:"success"`
}
type Result struct {
	Httpstatus    int           `json:"httpstatus"`
	Configuration Configuration `json:"configuration"`
	Message       string        `json:"message,omitempty"`
	Isactive      int           `json:"isactive,omitempty"`
	Isinvited     string        `json:"isinvited,omitempty"`
	Userinfo      *Userinfo     `json:"userinfo,omitempty"`
}
type Configuration struct {
	Mqttconfig [1]Mqttconfig `json:"mqttconfig"`
}
type Mqttconfig struct {
	Pi  string `json:"pi"`
	Nsu string `json:"nsu"`
	Dwp string `json:"dwp"`
}
type Userinfo struct {
	Email       string `json:"email,omitempty"`
	Firstname   string `json:"firstname,omitempty"`
	Lastname    string `json:"lastname,omitempty"`
	Phonenumber string `json:"phonenumber,omitempty"`
	Userimage   string `json:"userimage,omitempty"`
	Userid      string `json:"userid,omitempty"`
}
type Initial_Request struct {
	Isemail         int    `json:"isemail"`
	Loginsourceid   string `json:"loginsourceid"`
	LoginSourceType int    `json:"loginSourceType"`
	Password        string `json:"password"`
	Username        string `json:"username"`
}
