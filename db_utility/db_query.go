package db_utility

import (
	"database/sql"
	"flag"
	"fmt"
	"log"

	_ "github.com/denisenkom/go-mssqldb"
)

//GLOBAL VARIABLES

var (
	userid   = flag.String("U", "mymovecheck@srkqk9pxtz", "login_id")
	password = flag.String("P", "MoveCheck123", "password")
	server   = flag.String("S", "srkqk9pxtz.database.windows.net", "server_name[\\instance_name]")
	database = flag.String("d", "WAVELINKS_DEV", "db_name")
	DBC      *sql.DB
)

/*
Function Name : init()

Function Description :	This function is responsible for loading the database at once

Parameters -:	none
Return Value :	none
*/
func init() {

	dsn := "server=" + *server + ";user id=" + *userid + ";password=" + *password + ";database=" + *database
	var err error
	DBC, err = sql.Open("mssql", dsn)
	if err != nil {
		fmt.Println("Cannot connect: ", err.Error())
		return
	}
	err = DBC.Ping()
	if err != nil {
		fmt.Println("Cannot connect: ", err.Error())
		return
	}

}

/*
Function Name : DBScan_fn()

Function Description :	This function scans all columns of table and stores values of each row in an array and returns that array
Error/Exception Handling :<TBD>
Parameters -:	row *sql.Rows
Return Value :	cols []string, data [][]string
				r_err error
*/
func DBScan_fn(row *sql.Rows) (cols []string, data [][]string, r_err error) {
	//	logger.FuncName()
	cols, r_err = row.Columns()
	if r_err != nil {
		log.Println(r_err)
		//		r_err = errors.New("SYSR_DB_QUERY_GETCOLUMN_047_ERR")
		return
	}
	tmp_byte := make([][]byte, len(cols))
	tmp := make([]interface{}, len(cols))
	for i, _ := range tmp_byte {
		tmp[i] = &tmp_byte[i] // Put pointers to each string in the interface slice
	}
	for row.Next() {
		r_err = row.Scan(tmp...)
		if r_err != nil {
			log.Println(r_err)
			//			r_err = errors.New("SYSR_DB_SCAN_FAIL_048_ERR")
			return
		}
		rawResult := make([]string, len(cols))
		for i, _ := range tmp_byte {
			rawResult[i] = string(tmp_byte[i]) // Put pointers to each string in the interface slice
		}
		data = append(data, rawResult)
	}
	return
} // DBScan_fn()
